interface initApiConf {
  [prop: string]: string;
}

export class ApiConfig {
  private static instance: ApiConfig;
  protected __mongoDbURI: string;
  protected __postgresDbURI: string;

  private constructor(initial: initApiConf) {
    this.__mongoDbURI = initial?.defaultMongoDbURI ?? '';
    this.__postgresDbURI = initial?.defaultPostgresDbURI ?? '';
  }

  static getInstance(initial: initApiConf) {
    if (ApiConfig.instance) {
      return this.instance;
    }
    this.instance = new ApiConfig(initial);
    return this.instance;
  }

  public get mongoDbURI(): string {
    return this.__mongoDbURI;
  }
  public set mongoDbURI(v: string) {
    this.__mongoDbURI = v;
  }
  public get postgresDbURI(): string {
    return this.__postgresDbURI;
  }
  public set postgresDbURI(v: string) {
    this.__postgresDbURI = v;
  }
}
const envMongoUri: string = process.env?.MONGO_URI ?? '';
const envPostgresUri: string = process.env?.POSTGRES_URI ?? '';

export const apiConf = ApiConfig.getInstance({
  defaultMongoDbURI: envMongoUri,
  defaultPostgresDbURI: envPostgresUri,
});
