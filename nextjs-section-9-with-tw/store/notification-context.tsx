import { createContext, useState } from 'react';

interface INotification {
  title: string;
  message: string;
  status: string;
  isShown: boolean;
}

interface INotificationContext {
  notification: INotification;
  showNotification: () => void;
  hideNotification: () => void;
  setNotificationContent: (notification: INotification, show: boolean) => void;
}

const emptyFunction: () => void = () => {};

const initialContext = {
  notification: { title: '', message: '', status: '', isShown: false },
  showNotification: emptyFunction,
  hideNotification: emptyFunction,
  setNotificationContent: emptyFunction,
};

export const NotificationContext = createContext<INotificationContext>(initialContext);

export function NotificationContextProvider(props: any) {
  const [notification, setNotification] = useState(initialContext.notification);

  const showNotification = () => {
    console.log('set show to true');
    setNotification((prevState) => ({ ...notification, isShown: true }));
  };
  const hideNotification = () => {
    setNotification((prevState) => ({ ...notification, isShown: false }));
  };
  const setNotificationContent = (notification: INotification, show: boolean) => {
    setNotification((prevState) => ({ ...notification, isShown: show }));
  };

  return (
    <NotificationContext.Provider
      value={{
        notification,
        showNotification,
        hideNotification,
        setNotificationContent,
      }}
    >
      {props.children}
    </NotificationContext.Provider>
  );
}
