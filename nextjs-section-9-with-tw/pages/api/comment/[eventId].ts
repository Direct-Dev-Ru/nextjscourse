import * as afs from 'fs/promises';
import type { NextApiRequest, NextApiResponse } from 'next';
import { dbPathBuild, readDbFileData } from '../../../helper/api-utils';
import { validator } from '../../../helper/validator';
import { cyrb53 } from '../../../helper/hash';
import { apiConf } from '@/config/apiconfig';
import { logga } from '../../../helper/loging/logga';
import { dbUtilsAnswer } from '@/helper/interfaces/dbInterfaces';
import { pgApi } from '@/helper/postgres/db-utils';

export default async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
  const eventId: string | string[] = req?.query?.eventId ?? undefined;

  try {
    const commentsQueryResult: dbUtilsAnswer = await pgApi.selectData(
      [{ relation: 'public.events_comments', alias: 'EC' }],
      [{ field: '*' }],
      { eventid: eventId, '!eventid': '' },
      //   `eventid='${eventId}'`
      //   [
      //     {
      //       field: 'eventid',
      //       value: eventId,
      //       operation: '=',
      //       //   logicalOperator: ,
      //       //   leftBrase: false,
      //       //   rightBrase: false,
      //     },
      //   ]
      [
        { field: 'created_at', direction: -1 },
        { field: 'email', direction: 1 },
      ]
    );

    if (commentsQueryResult?.isError) {
      return res.status(500).json({
        error: true,
        status: '500',
        message: commentsQueryResult?.error?.message ?? 'db reading error',
        payload: { error: commentsQueryResult?.error },
      });
    }

    const commentsData = commentsQueryResult?.result;

    if (req.method === 'POST') {
      const { email, name, text } = req.body;

      //   validation ... better to use sanitaize
      if (!validator('email', email)) {
        return res
          .status(422)
          .json({ error: true, status: '422', message: 'email validation error', payload: { email } });
      }
      if (!validator('name', name)) {
        return res.status(422).json({
          error: true,
          status: '422',
          message: 'name validation error: only letters from 3 and more ...',
          payload: { name },
        });
      }
      //   if in text var there are tags - break ...
      const findTags = validator('custom', text, /(<([^>]+)>)/gi);
      if (findTags || !validator('empty', text)) {
        return res.status(422).json({
          error: true,
          status: '422',
          message: 'text validation error: some tags find or text is empty',
          payload: { findTags },
        });
      }
      //   end if validation

      let newComment = {
        eventId,
        email: email.trim(),
        name: name.trim(),
        text: text.trim(),
        hash: cyrb53(eventId + email.trim() + text.trim()),
      };

      //  check for duplicates
      if (commentsData.find((comment: any) => comment.hash === newComment.hash)) {
        return res
          .status(422)
          .json({ error: true, status: '422', message: 'duplicate comment', payload: { newComment } });
      }

      //   write to Db

      const resultOfInsertion: dbUtilsAnswer = await pgApi.insertOne('public.events_comments', newComment);

      if (resultOfInsertion?.isError) {
        return res.status(500).json({
          error: true,
          status: '500',
          message: resultOfInsertion?.error?.message ?? 'db insertion error',
          payload: { error: resultOfInsertion?.error },
        });
      }
      newComment = resultOfInsertion.result;

      return res.status(201).json({
        error: false,
        status: '201',
        message: 'Successfully added new comment',
        payload: { newComment },
      });
    }

    if (req.method === 'GET') {
      return res.status(200).json({
        error: false,
        status: '200',
        message: `Successfully reading comments for eventId = ${eventId}`,
        payload: { data: commentsData },
      });
    }
  } catch (e: any) {
    return res
      .status(500)
      .json({ error: true, status: '500', message: e?.message ?? 'internal error', payload: { error: e } });
  } finally {
    // Ensures that the client will close when you finish/error
    // await dbClient?.close();
  }

  return res.status(404).send({ error: true, status: '404', message: 'No such method in api' });
}
