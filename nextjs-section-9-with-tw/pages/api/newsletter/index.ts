import { logga } from '@/helper/loging/logga';
import type { NextApiRequest, NextApiResponse } from 'next';
import { validator } from '../../../helper/validator';
import { pgApi } from '@/helper/postgres/db-utils';

export default async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
  if (req.method === 'POST') {
    const userEmail: string | undefined = req?.body?.email ?? undefined;

    if (!validator('email', userEmail)) {
      return res
        .status(422)
        .json({ error: true, status: '422', message: 'validation error', payload: { email: userEmail } });
    }

    try {
      const resultOfInsertion = await pgApi.insertOne('public.events_emails', { email: userEmail });
      if (resultOfInsertion.isError) {
        return res.status(500).json({
          error: true,
          status: '500',
          message: resultOfInsertion.error?.message ?? 'database error',
          payload: { error: resultOfInsertion.error },
        });
      }
      return res.status(201).json({
        error: false,
        status: '201',
        message: 'Successfully added newsletter subscribtion',
        payload: { newSubscription: resultOfInsertion.result[0] },
      });
    } catch (e: any) {
      return res
        .status(500)
        .json({ error: true, status: '500', message: e?.message ?? 'internal error', payload: { error: e } });
    } finally {
    }
  }

  if (req.method === 'GET') {
    const dummyA: any[] = [];
    try {
      const data = await pgApi.selectData(
        [{ relation: 'public.events_emails', alias: '' }],
        [{ field: 'id' }, { field: 'email', alias: 'mail' }],
        [
          {
            field: 'id',
            value: '2',
            operation: '=',
            logicalOperator: undefined,
            leftBrase: true,
            rightBrase: false,
          },
          {
            field: 'email',
            value: '%info%',
            operation: 'LIKE',
            logicalOperator: ' OR ',
            leftBrase: false,
            rightBrase: true,
          },
        ],
        [{ field: 'id', direction: -1 }]
      );
      if (data.isError) {
        return res.status(500).json({
          error: true,
          status: '500',
          message: data.error?.message ?? 'database error',
          payload: { error: data.error },
        });
      }

      return res.status(200).json({
        error: false,
        status: '200',
        message: 'Successfully reading current newsletter subscriptions',
        payload: { data },
      });
    } catch (e: any) {
      return res
        .status(500)
        .json({ error: true, status: '500', message: e?.message ?? 'internal error', payload: { error: e } });
    }
  }
  return res.status(404).send({ error: true, status: '404', message: 'No such method in api' });
}
