import { MongoClient } from 'mongodb';
import { dbUtilsAnswer } from '@/helper/interfaces/dbInterfaces';

export async function connectMongo(mongoDbURI: string): Promise<dbUtilsAnswer> {
  try {
    const client = new MongoClient(mongoDbURI);
    await client.connect();
    await client.db().command({ ping: 1 });
    const result: dbUtilsAnswer = { isError: false, error: undefined, result: client };
    return result;
  } catch (error: any) {
    return { isError: true, error, result: undefined };
  }
}

export async function insertDocument(clientOrURI: any, collection: string, document: any): Promise<dbUtilsAnswer> {
  try {
    let client;
    if (clientOrURI?.db()) {
      client = clientOrURI;
    } else {
      client = new MongoClient(clientOrURI);
      await client.connect();
      await client.db().command({ ping: 1 });
    }
    const db = client.db();
    const dbCollection = db.collection(collection);

    const insertedDocument = await dbCollection.insertOne(document);
    document._id = insertedDocument.insertedId.toString();
    const result: dbUtilsAnswer = { isError: false, error: undefined, result: document };
    return result;
  } catch (error: any) {
    return { isError: true, error, result: undefined };
  }
}

export async function getMongoDocuments(
  clientOrURI: any,
  collection: string,
  sort: object,
  filter = {}
): Promise<dbUtilsAnswer> {
  let resultArray: any[] = [];
  let result: any;
  let client;

  let disconnect = false;
  try {
    if (clientOrURI?.db()) {
      client = clientOrURI;
    } else {
      client = new MongoClient(clientOrURI);
      await client.connect();
      await client.db().command({ ping: 1 });
      disconnect = true;
    }

    const db = client.db();
    const dbCollection = db.collection(collection);
    resultArray = await dbCollection.find(filter).sort(sort).toArray();
    result = { isError: false, error: undefined, result: resultArray };
  } catch (error: any) {
    result = { isError: true, error, result: undefined };
  } finally {
    if (disconnect) {
      client.close();
    }
  }
  return result;
}
