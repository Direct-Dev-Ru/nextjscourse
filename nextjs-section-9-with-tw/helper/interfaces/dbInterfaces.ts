export interface dbUtilsAnswer {
  isError: Boolean;
  error: Error | undefined;
  result: any;
}

export interface IRelationEntry {
  relation: string;
  // 1=ASC, -1=DESC
  alias?: string;
}
export interface ISortEntry {
  field: string;
  direction?: number; // 1=ASC, -1=DESC
}
export interface IFieldEntry {
  field: string;
  alias?: string;
}
export interface IFilterEntry {
  field: string;
  value: any;
  operation?: string;
  logicalOperator?: string;
  leftBrase?: boolean;
  rightBrase?: boolean;
}

export interface dbApi {
  connect(): Promise<dbUtilsAnswer>;

  //   insertOne(relation: string, fields: string[], values: any[]): Promise<dbUtilsAnswer>;
  insertOne(relation: string, insertObject: any): Promise<dbUtilsAnswer>;

  selectData(
    relation: IRelationEntry[],
    fieldsObject?: IFieldEntry[],
    filterObject?: IFilterEntry[] | string | object,
    sortSequence?: ISortEntry[]
    // relation: IRelationEntry[] | string | object,
    // fieldsObject?: IFieldEntry[] | string | object,
    // filterObject?: IFilterEntry[] | string | object,
    // sortSequence?: ISortEntry[] | string | object
  ): Promise<dbUtilsAnswer>;
}
