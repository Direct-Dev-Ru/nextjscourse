export const isDev: boolean = process.env.NODE_ENV === 'development';

export const logga: (message: any, ...restMessages: any[]) => void = (message: any, ...restMessages: any[]) => {
  if (isDev) {
    console.log(message);
    for (const mes of restMessages) {
      console.log(mes);
    }
  }
};

// logga(process.env.MONGO_URI);
