import { Pool } from 'pg';
import {
  dbUtilsAnswer,
  dbApi,
  ISortEntry,
  IFieldEntry,
  IFilterEntry,
  IRelationEntry,
} from '@/helper/interfaces/dbInterfaces';
import { apiConf } from '@/config/apiconfig';
import { logga } from '@/helper/loging/logga';
class PgApi implements dbApi {
  private static instance: PgApi;
  protected __pool: Pool;

  private constructor(connectionString: string) {
    this.__pool = new Pool({
      connectionString,
      max: 20,
      idleTimeoutMillis: 30_000,
      connectionTimeoutMillis: 2000,
    });
  }

  static getInstance(connectionString: string): PgApi {
    if (PgApi.instance) {
      return this.instance;
    }
    this.instance = new PgApi(connectionString);
    return this.instance;
  }

  async connect(): Promise<dbUtilsAnswer> {
    try {
      const client = await this.__pool.connect();
      const result: dbUtilsAnswer = { isError: false, error: undefined, result: client };
      return result;
    } catch (error: any) {
      return { isError: true, error, result: undefined };
    }
  }

  async insertOne(relation: string, insertObject: any): Promise<dbUtilsAnswer> {
    let fieldString: string = '';
    let valueString: string = '';
    let values: any[] = [];
    let i = 1;

    for (const key in insertObject) {
      const element = insertObject[key as keyof typeof insertObject];
      fieldString += i === 1 ? key : `, ${key}`;
      valueString += `${i === 1 ? '' : `,`}$${i}`;
      values.push(element);

      i++;
    }

    const text = `INSERT INTO ${relation} (${fieldString}) VALUES(${valueString}) RETURNING *`;
    logga(text);
    const client = await this.__pool.connect();
    try {
      //
      await client.query('BEGIN');
      const inserted = await client.query(text, values);
      const result: dbUtilsAnswer = { isError: false, error: undefined, result: inserted.rows };
      await client.query('COMMIT');
      return result;
      //
    } catch (error: any) {
      //
      await client.query('ROLLBACK');
      return { isError: true, error, result: undefined };
      //
    } finally {
      //
      client.release();
      //
    }
  }

  async selectData(
    relationSequence: IRelationEntry[],
    fieldsSequence?: IFieldEntry[],
    filterSequence?: IFilterEntry[] | string | object,
    sortSequence?: ISortEntry[]
  ): Promise<dbUtilsAnswer> {
    let relationString: string = '';
    let fieldString: string = '';
    let filterString: string = '';
    let filterValues: any[] = [];
    let sortString: string = '';

    let i = 1;
    if (relationSequence) {
      for (const relationEntry of relationSequence) {
        const { relation, alias } = relationEntry as IRelationEntry;
        const relationMember = relation ? `${relation} ${alias ? ` AS ${alias}` : ``}` : ``;
        relationString += `${i === 1 ? '' : ','} ${relationMember}`;
        i++;
      }
    } else {
      return { isError: true, error: new Error('empty relations sequence'), result: undefined };
    }

    if (fieldsSequence) {
      i = 1;
      for (const fieldEntry of fieldsSequence) {
        const { field, alias } = fieldEntry as IFieldEntry;
        const fieldMember = field ? `${field}${alias ? ` AS ${alias}` : ``}` : ``;
        fieldString += `${i === 1 ? '' : ','} ${fieldMember}`;
        i++;
      }
    } else {
      fieldString = '*';
    }

    if (filterSequence) {
      i = 1;
      if (Array.isArray(filterSequence)) {
        for (const filterEntry of filterSequence) {
          const { field, operation, value, logicalOperator, leftBrase, rightBrase } = filterEntry as IFilterEntry;
          const filterMember = field ? `${field} ${operation ? `${operation}` : `=`} $${i} ` : `1=1`;
          if (field && value) {
            filterValues.push(value);
          }
          filterString += `${logicalOperator ? `${logicalOperator}` : ``} ${leftBrase ? '(' : ''} ${filterMember} ${
            rightBrase ? ')' : ''
          }`;
          i++;
        }
      } else if (typeof filterSequence === 'string') {
        filterString = filterSequence.trim();
      } else if (typeof filterSequence === 'object') {
        for (let field in filterSequence) {
          if (Object.prototype.hasOwnProperty.call(filterSequence, field)) {
            const nego: boolean = field.startsWith('!');

            const value: any = filterSequence[field as keyof typeof filterSequence];

            let op = '=';
            let novalue = 'IS NULL';

            if (nego) {
              op = '<>';
              novalue = 'IS NOT NULL';
              field = field.substring(1);
            }

            const filterMember: string = field ? `${field}${value ? `${op}$${i}` : ` ${novalue}`}` : ``;
            if (field && value) {
              filterValues.push(value);
            }
            filterString += `${i === 1 ? '' : ' AND '}${filterMember}`;
            i++;
          }
        }
      }
    }

    if (sortSequence) {
      i = 1;
      for (const sortEntry of sortSequence) {
        const { field, direction } = sortEntry as ISortEntry;
        const sortMember = field ? `${field}${direction === -1 ? ' DESC' : ' ASC'}` : '';
        sortString += `${i === 1 ? '' : ', '}${sortMember}`;
        i++;
      }
    }

    const text = `

    SELECT ${fieldString} 
    FROM ${relationString} 
    ${filterString ? `WHERE ${filterString}` : ''} 
    ${sortString ? `ORDER BY ${sortString}` : ''}

    `.trim();
    logga(text);

    const client = await this.__pool.connect();
    try {
      const selectResult = await client.query(text, filterValues);

      const result: dbUtilsAnswer = { isError: false, error: undefined, result: selectResult.rows };
      return result;
    } catch (error: any) {
      return { isError: true, error, result: undefined };
    } finally {
      client.release();
    }
  }
}

export const pgApi = PgApi.getInstance(apiConf.postgresDbURI);
