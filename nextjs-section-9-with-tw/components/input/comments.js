import { useState, useEffect, useContext } from 'react';
import { logga } from '@/helper/loging/logga';
import CommentList from './comment-list';
import NewComment from './new-comment';
import classes from './comments.module.css';
import { NotificationContext } from '@/store/notification-context';
import Circle from '@/components/ui/Circle';
// import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

function Comments(props) {
  const { eventId } = props;

  const [showComments, setShowComments] = useState(false);
  const [comments, setComments] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  //   notification Context
  const notificationCtx = useContext(NotificationContext);
  const { notification, setNotificationContent } = notificationCtx;
  const { title, message, status, isShown } = notification;
  function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  useEffect(async () => {
    if (showComments) {
      let isApiSubscribed = true;
      const controller = new AbortController();
      const signal = controller.signal;

      const returnFunction = () => {
        isApiSubscribed = false;
        controller.abort();
      };

      try {
        setNotificationContent(
          {
            title: 'Getting comments ...',
            message: 'Reading current comments for event...',
            status: 'pending',
            isShown: showComments,
          },
          showComments
        );
        setIsLoading(true);

        await delay(3_000);

        const res = await fetch(`/api/comment/${eventId}`, {
          signal,
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        });
        const data = await res.json();
        if (!res.ok) {
          logga(`error :${res.status}`);
          setNotificationContent(
            {
              title: 'Error getting comments ...',
              message: error.message,
              status: `error :${res.status}`,
              isShown: showComments,
            },
            showComments
          );
          setIsLoading(false);
          return returnFunction;
        }
        setComments(data?.payload?.data ?? []);
        setNotificationContent(
          {
            title: 'Getting comments ...',
            message: 'Successfully read comments',
            status: 'success',
            isShown: showComments,
          },
          showComments
        );
        setIsLoading(false);
      } catch (error) {
        setNotificationContent(
          {
            title: 'Error getting comments ...',
            message: error.message,
            status: 'error',
            isShown: showComments,
          },
          showComments
        );
        setIsLoading(false);
        logga(error);
      }
      return returnFunction;
    }
  }, [showComments]);

  function toggleCommentsHandler() {
    setShowComments((prevStatus) => !prevStatus);
  }

  async function addCommentHandler(commentData, setInfo) {
    try {
      const response = await fetch(`/api/comment/${eventId}`, {
        method: 'POST',
        body: JSON.stringify(commentData),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      });
      if (response.ok) {
        const data = await response.json();
        setInfo((prev) => {
          return {
            isShow: true,
            isInvalid: false,
            message: `Comment added successfully`,
          };
        });
        setNotificationContent(
          {
            title: 'Success post comment!',
            message: `Comment added successfully`,
            status: 'success',
            isShown: true,
          },
          true
        );

        const newComments = [...data?.payload?.newComment, ...comments];
        setComments(newComments);
      } else {
        setInfo((prev) => {
          return {
            isShow: true,
            isInvalid: true,
            message: `error occured while processing new comment post request status:${response.status}`,
          };
        });
        setNotificationContent(
          {
            title: 'Error post comment!',
            message: `error occured while processing new comment post request status:${response.status}`,
            status: 'error',
            isShown: true,
          },
          true
        );
        // console.log(`error occured while processing new comment post request status:${response.status}`);
      }
    } catch (error) {
      //   console.log(error);
      setNotificationContent(
        {
          title: 'Error getting comments ...',
          message: error.message,
          status: 'error',
          isShown: true,
        },
        true
      );
    }
  }

  return (
    <section className={classes.comments}>
      <button onClick={toggleCommentsHandler}>{showComments ? 'Hide' : 'Show'} Comments</button>
      {showComments && <NewComment onAddComment={addCommentHandler} />}
      {showComments && isLoading ? <Circle color='red' size={150} /> : <CommentList items={comments} />}
    </section>
  );
}

export default Comments;
