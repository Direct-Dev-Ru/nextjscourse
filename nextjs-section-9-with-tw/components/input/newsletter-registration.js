import classes from './newsletter-registration.module.css';
import { useRef, useContext } from 'react';
import { validator } from '../../helper/validator';

import { NotificationContext } from '@/store/notification-context';

function NewsletterRegistration(props) {
  const { setError } = props;

  const emailInputRef = useRef();
  //   notification Context
  const notificationCtx = useContext(NotificationContext);
  const { notification, hideNotification, setNotificationContent } = notificationCtx;
  const { title, message, status, isShown } = notification;

  async function registrationHandler(ev) {
    ev.preventDefault();

    const enteredEmail = emailInputRef.current.value;

    if (!validator('email', enteredEmail)) {
      if (setError) {
        setError((prevState) => ({ ...prevState, errorTitle: 'Bad email - failed client validation', isError: true }));
      }
      return;
    }

    try {
      setNotificationContent(
        { title: 'Signing up ...', message: 'Registering for newsletters', status: 'pending', isShown: true },
        true
      );
      const res = await fetch('/api/newsletter', {
        method: 'POST',
        body: JSON.stringify({ email: enteredEmail }),
        headers: { 'Content-Type': 'application/json' },
      });
      const data = await res.json();
      if (data?.error) {
        setNotificationContent({ title: 'Error!', message: data?.message, status: 'error', isShown: true }, true);
        emailInputRef.current.focus();
        return;
      }
      setNotificationContent(
        { title: 'Signing up ...', message: 'Successfully register for newsletters', status: 'success', isShown: true },
        true
      );
      emailInputRef.current.value = '';
    } catch (e) {
      setNotificationContent({ title: 'Error!', message: e?.message, status: 'error', isShown: true }, true);
    }
  }

  return (
    <section className={classes.newsletter}>
      <h2>Sign up to stay updated!</h2>
      <form onSubmit={registrationHandler}>
        <div className={classes.control}>
          <input ref={emailInputRef} type='email' id='email' placeholder='Enter Your email' aria-label='Your email' />
          <button>Register</button>
        </div>
      </form>
    </section>
  );
}

export default NewsletterRegistration;
