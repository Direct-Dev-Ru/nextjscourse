/// <reference types="react" />
import type { Props } from '../types';
export default function Circle({ color, size, className, style, ...rest }: Props): JSX.Element;
//# sourceMappingURL=index.d.ts.map