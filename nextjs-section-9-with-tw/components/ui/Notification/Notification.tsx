import { useContext, useEffect } from 'react';

import classes from './notification.module.css';
import { NotificationContext } from '@/store/notification-context';
import { logga } from '@/helper/loging/logga';

function Notification(props: any) {
  const notificationCtx = useContext(NotificationContext);
  const { notification, hideNotification } = notificationCtx;

  const { title, message, status } = props;

  useEffect(() => {
    if (status !== 'pending' && notification.isShown) {
      logga('setting clearing interval');
      const timerHide = setTimeout(() => props.closeHandler(), 5_000);
      return () => {
        logga('clear timeout');
        clearTimeout(timerHide);
      };
    }
  }, [notification]);

  let hiddenClasses = '';
  if (!props.isShown) {
    hiddenClasses = classes.hidden;
  }

  let statusClasses = '';
  let buttonClasses = '';
  if (status === 'success') {
    statusClasses = classes.success;
    buttonClasses = `bg-transparent hover:bg-green-600 text-white-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded`;
  }
  if (status === 'error') {
    statusClasses = classes.error;
    buttonClasses = `bg-transparent hover:bg-red-600 text-white-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded`;
  }
  if (status === 'pending') {
    statusClasses = classes.pending;
    buttonClasses = `bg-transparent hover:bg-blue-500 text-white-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded`;
  }

  const activeClasses = `${classes.notification} ${statusClasses} ${hiddenClasses}`;

  return (
    <div className={activeClasses} onClick={notificationCtx.hideNotification}>
      <h2>{title}</h2>
      <p>{message}</p>
      <button type='button' className={buttonClasses} onClick={props.closeHandler}>
        Close
      </button>
    </div>
  );
}

export default Notification;
