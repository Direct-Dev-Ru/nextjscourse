import React, { Fragment, FunctionComponent, useState, useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import HtmlHead from './HtmlHead';
import HtmlFooter from './HtmlFooter';
import MainHeader from './MainHeader';
import Notification from '@/components/ui/Notification/Notification';
import apiConfig from '../../config/config';

import { isDev, logga } from 'helper/loging/logga';
import { NotificationContext } from '@/store/notification-context';

const ParentLayout: FunctionComponent<any> = ({ children }) => {
  const routerMain = useRouter();
  //   apiConfig.URL = 'http://localhost:3000/api/';
  const [render, setRender] = useState(false);
  const childrenWithProps = React.Children.map(children, (child, index) =>
    React.cloneElement(child, {
      reRenderFromLayout: () => {
        setRender(!render);
      },
      render,
      isDev,
      logga,
      router: routerMain,
    })
  );
  //   notification Context
  const notificationCtx = useContext(NotificationContext);
  const { notification, showNotification, hideNotification, setNotificationContent } = notificationCtx;
  const { title, message, status, isShown } = notification;

  useEffect(() => {
    setNotificationContent({ title: 'Cookies', message: 'We use cookies !!!', status: 'pending', isShown: true }, true);
    const timer1 = setTimeout(() => hideNotification(), 10_000);
    return () => {
      clearTimeout(timer1);
    };
  }, []);

  //   const router = useRouter();

  return (
    <Fragment>
      <MainHeader />
      <main className='flex flex-col items-center justify-center w-full flex-1 px-20 text-center'>
        {childrenWithProps}
        <Notification
          title={title}
          message={message}
          status={status}
          isShown={isShown}
          closeHandler={hideNotification}
        />
      </main>
      <HtmlFooter />
    </Fragment>
  );
};

export default ParentLayout;

function Layout(props: any) {
  const router = useRouter();

  if (router.route === '/tailwind') {
    return <Fragment> {props.children} </Fragment>;
  }

  return (
    <Fragment>
      <MainHeader />
      <main className='flex flex-col items-center justify-center w-full flex-1 px-20 text-center'>
        {props.children}
      </main>

      <HtmlFooter />
    </Fragment>
  );
}
